# B2 Linux 2022

Vous trouverez ici les ressources liées au cours autour de GNU/Linux de deuxième année.

### ➜ **[Cours](./cours/README.md)**

- [SSH](./cours/SSH/README.md)
- [Partitionnement](./cours/partition/README.md)
- [Processus](./cours/process/README.md)
- [Ansible (abordé en soutien projet)](./cours/ansible/README.md)

### ➜ **[TP](./tp/README.md)**

- [TP 1 : Mise en jambe](./tp/1/README.md)
- [TP 2 : Gestion de services](./tp/2/README.md)
- [TP 3 : Amélioration de la solution NextCloud](./tp/3/README.md)
- [TP 4 : Conteneurs](./tp/4/README.md)
- [TP 5 : Hébergement d'une solution libre et opensource](./tp/5/README.md)
- [Sujet Rattrapage B2](./tp/repech/README.md)

### ➜ **[Mémos](./cours/memos/README.md)**

- [Mémo LVM](./cours/memos/lvm.md)
- [Mémo commandes](./cours/memos/commandes.md)
- [Mémo réseau Rocky Linux](./cours/memos/rocky_network.md)
