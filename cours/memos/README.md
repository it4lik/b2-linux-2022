# Mémos

- [Install Rocky](./install_vm.md)
- [Commandes générales GNU/Linux](./commandes.md)
- [Rocky Network](./rocky_network.md)
- [LVM](./lvm.md)