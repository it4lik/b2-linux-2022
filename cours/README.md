# Cours

### ➜ Cours

- [SSH](./SSH/README.md)
- [Partitionnement](./partition/README.md)
- [Processus](./process/README.md)
- [Ansible (abordé en soutien projet)](./ansible/README.md)

### ➜ Mémos

- [Mémo LVM](./memos/lvm.md)
- [Mémo commandes](./memos/commandes.md)
- [Mémo réseau Rocky Linux](./memos/rocky_network.md)
