# Intro : Ansible chill

➜ ***Ansible* est utilisé pour déposer de la configuration sur les machines** que l'on aura préalablement créées.

## Sommaire

- [Intro : Ansible chill](#intro--ansible-chill)
  - [Sommaire](#sommaire)
- [I. Ansible](#i-ansible)
  - [1. Mise en place](#1-mise-en-place)
  - [2. La commande `ansible`](#2-la-commande-ansible)
  - [3. Un premier playbook](#3-un-premier-playbook)
  - [3. Création de nouveaux playbooks](#3-création-de-nouveaux-playbooks)
    - [A. NGINX](#a-nginx)
    - [B. MariaDB](#b-mariadb)

# I. Ansible

➜ Pour qu'Ansible fonctionne il nous faut :

- du code Ansible
- des machines sur lesquelles déposer de la conf
  - ce sera des machines Rocky
  - Ansible et Python installés
  - un serveur SSH dispo
- une machine qui possède le code Ansible
  - ce sera votre PC
  - cette machine doit pouvoir se connecter sur les machines de destination en SSH
  - utilisation d'un utilisateur qui a accès aux droits `root` via la commande `sudo`

Une fois en place, on pourra rédiger nos premiers *playbooks*.

## 1. Mise en place

➜ **Créez 2 VMs Rocky Linux**

- chacune doit avoir une adresse IP privée dans le réseau `10.1.1.0/24`
  - une en `10.1.1.11` l'autre en `10.1.1.12`
- modifiez le script `setup.sh` pour que les VMs créées :
  - aient Ansible et Python installés
  - aient un user qui a accès aux droits `root` avec la commande `sudo`
    - je vous conseille une conf en `NOPASSWD` pour ne pas avoir à saisir votre password à chaque déploiement
  - ce user a une clé publique pour vous y connecter sans mot de passe

➜ **Installez Ansible sur votre machine**

- il vous faudra aussi Python
- suivez [la doc officielle pour l'install](https://docs.ansible.com/ansible/latest/installation_guide/index.html)

➜ **Créez un nouveau répertoire `tp1/ansible/` dans votre dépôt git**

- il accueillera le code Ansible pour ce TP

> ***N'hésitez pas à me demander de l'aide pour le setup.***

## 2. La commande `ansible`

On va pouvoir utiliser un peu Ansible !

➜ Pour cela, **créez un fichier `hosts.ini`** avec le contenu suivant :

```ini
[tp1]
10.1.1.11
10.1.1.12
```

On va commencer avec quelques commandes Ansible pour exécuter des tâches *ad-hoc* : c'est à dire des tâches one shot depuis la ligne de commandes.

```bash
# lister les hôtes que Ansible voit dans notre inventaire
$ ansible -i hosts.ini tp1 --list-hosts

# tester si ansible est capable d'interagir avec les machines
$ ansible -i hosts.ini tp1 -m ping

# afficher toutes les infos que Ansible est capable de récupérer sur chaque machine
$ ansible -i hosts.ini tp1 -m setup

# exécuter une commande sur les machines distantes
$ ansible -i hosts.ini tp1 -m command -a 'uptime'

# exécuter une commande en root
$ ansible -i hosts.ini tp1 --become -m command -a 'reboot'
```

## 3. Un premier playbook

Enfin, on va écrire un peu de code Ansible.  
On va commencer simple et faire un *playbook* en un seul fichier, pour prendre la main sur la syntaxe, et faire un premier déploiement.

➜ **créez un fichier `first.yml`**, notre premier *playbook* :

```yaml
---
- name: Install nginx
  hosts: tp1
  become: true

  tasks:
  - name: Install nginx
    dnf:
      name: nginx
      state: present

  - name: Insert Index Page
    template:
      src: index.html.j2
      dest: /usr/share/nginx/html/index.html

  - name: Start NGiNX
    service:
      name: nginx
      state: started
```

> Chaque élément de cette liste YAML est donc une *task* Ansible. Les mots-clés `yum`, `template`, `service` sont des *modules* Ansible.

➜ **Et un fichier `index.html.j2` dans le même dossier**

```jinja2
Hello from {{ ansible_default_ipv4.address }}
```

➜ **Exécutez le playbook**

```bash
$ ansible-playbook -i hosts.ini first.yml
```

## 3. Création de nouveaux playbooks

### A. NGINX

➜ **Créez un *playbook* `nginx.yml`**

- déploie un serveur NGINX
- génèreérer un certificat et une clé au préalable
  - le certificat doit être déposé dans `/etc/pki/tls/certs`
  - la clé doit être déposée dans `/etc/pki/tls/private`
- créer une racine web et un index
  - créez le dossier `/var/www/tp1_site/`
  - créez un fichier à l'intérieur `index.html` avec un contenu de test
- déploie une nouveau fichier de conf NGINX
  - pour servir votre `index.html` en HTTPS (port 443)
- ouvre le port 443/TCP dans le firewall

➜ **Modifiez votre `hosts.ini`**

- ajoutez une section `web`
- elle ne contient que `10.1.1.11`

➜ **Lancez votre playbook sur le groupe `web`**

➜ **Vérifiez que vous accéder au site avec notre navigateur**

> S'il y a besoin d'aide pour tout ça, si vous n'êtes pas du tout familier avec la conf NGINX, n'hésitez pas à faire appel à moi.

### B. MariaDB

➜ **Créez un *playbook* `mariadb.yml`**

- déploie un serveur MariaDB
- créer un user SQL ainsi qu'une base de données sur laquelle il a tous les droits

➜ **Modifiez votre `hosts.ini`**

- ajoutez une section `db`
- elle ne contient que `10.1.1.12`

➜ **Lancez votre playbook sur le groupe `db`**

➜ **Vérifiez en vous connectant à la base que votre conf a pris effet**
