# Ansible

Suite à la demande des étudiants, un point sur Ansible a été abordé durant les cours de soutien projet.

En voilà une brève trace écrite.

TP :

- [Intro](./tp/intro/README.md)
- [Structure de dépôt](./tp/structure/README.md)

![Ansible draw.io](./pics/ansible.svg)
