
| nom          | choix | bonnes pratiques | fonctionne ? | services additionels | qualité doc | note |
|--------------|-------|------------------|--------------|----------------------|-------------|------|
| barême       | 3     | 5                | 2            | 5                    | 5           | 20   |
| bedja        | 3     | 3                | 3            | 0                    | 2           | 11   |
| biret        | 3     | 3                | 0            | 0                    | 3           | 9    |
| BOURMAUD     | 3     | 5                | 2            | 3                    | 5           | 18   |
| bousseria    | 2     | 4                | 1            | 0                    | 2           | 9    |
| bravo        | 3     | 4                | 2            | 3                    | 4           | 16   |
| brun         | 3     | 5                | 2            | 4                    | 4           | 18   |
| chalaye      | 3     | 5                | 2            | 1                    | 5           | 16   |
| corre        | 3     | 3                | 2            | 2                    | 3           | 12   |
| coutant      | 1     | 0                | 0            | 0                    | 0           | 1    |
| dalamel      | 2     | 4                | 1            | 0                    | 2           | 9    |
| doublait     | 3     | 5                | 2            | 3                    | 5           | 18   |
| duniaud      | 3     | 3                | 2            | 4                    | 4           | 16   |
| durand e     | 3     | 5                | 2            | 5                    | 3           | 18   |
| eveillard    | 3     | 4                | 2            | 5                    | 5           | 19   |
| ferreira     | 3     | 3                | 2            | 2                    | 4           | 14   |
| gadebille    | 3     | 4                | 2            | 5                    | 5           | 19   |
| garcia f     | 3     | 4                | 2            | 5                    | 5           | 19   |
| garcia l     | 2     | 4                | 2            | 5                    | 5           | 19   |
| garrabos     | 3     | 5                | 2            | 1                    | 5           | 16   |
| gilles       | 3     | 4                | 0            | 0                    | 3           | 10   |
| grand morcel | 3     | 5                | 2            | 5                    | 5           | 20   |
| guillet      | 0     | 0                | 0            | 0                    | 0           | 0    |
| jovanovic    | 0     | 0                | 0            | 0                    | 0           | 0    |
| juillard     | 3     | 4                | 2            | 3                    | 3           | 11   |
| laroumanie   | 3     | 5                | 2            | 3                    | 5           | 18   |
| latorre      | 3     | 5                | 2            | 3                    | 5           | 18   |
| lavaud       | 2     | 4                | 1            | 0                    | 2           | 9    |
| leconte      | 3     | 3                | 2            | 5                    | 3           | 16   |
| lopez t      | 3     | 0                | 2            | 0                    | 5           | 10   |
| magne        | 1     | 0                | 0            | 0                    | 0           | 1    |
| menard       | 2     | 2                | 2            | 0                    | 2           | 8    |
| moiska       | 3     | 5                | 2            | 4                    | 3           | 17   |
| morand       | 0     | 3                | 2            | 0                    | 5           | 10   |
| pajak        | 3     | 5                | 2            | 5                    | 3           | 18   |
| pery         | 0     | 0                | 0            | 0                    | 0           | 0    |
| poirier      | 3     | 2                | 2            | 1                    | 5           | 13   |
| Porte        | 3     | 5                | 2            | 3                    | 5           | 18   |
| pougeard     | 0     | 0                | 0            | 0                    | 0           | 0    |
| renzema      | 0     | 0                | 0            | 0                    | 0           | 10   |
| revol        | 3     | 4                | 2            | 3                    | 4           | 16   |
| roulland     | 3     | 5                | 2            | 5                    | 4           | 19   |
| ruemeli      | 3     | 3                | 2            | 5                    | 3           | 16   |
| selva        | 3     | 3                | 2            | 2                    | 4           | 14   |
| senhaj       | 3     | 5                | 2            | 0                    | 0           | 10   |
| thebault     | 3     | 3                | 2            | 0                    | 4           | 12   |
| toribio      | 3     | 3                | 2            | 0                    | 4           | 12   |
| toulemon     | 3     | 3                | 2            | 4                    | 4           | 16   |
| vasseur      | 3     | 5                | 2            | 5                    | 4           | 19   |
| weber        | 3     | 2                | 2            | 1                    | 5           | 13   |
| wora         | 3     | 5                | 2            | 4                    | 4           | 18   |
