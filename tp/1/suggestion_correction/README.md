# TP1 : (re)Familiaration avec un système GNU/Linux

## Sommaire

- [TP1 : (re)Familiaration avec un système GNU/Linux](#tp1--refamiliaration-avec-un-système-gnulinux)
  - [Sommaire](#sommaire)
  - [0. Préparation de la machine](#0-préparation-de-la-machine)
  - [I. Utilisateurs](#i-utilisateurs)
    - [1. Création et configuration](#1-création-et-configuration)
    - [2. SSH](#2-ssh)
  - [II. Partitionnement](#ii-partitionnement)
    - [1. Préparation de la VM](#1-préparation-de-la-vm)
    - [2. Partitionnement](#2-partitionnement)
  - [III. Gestion de services](#iii-gestion-de-services)
  - [1. Interaction avec un service existant](#1-interaction-avec-un-service-existant)
  - [2. Création de service](#2-création-de-service)
    - [A. Unité simpliste](#a-unité-simpliste)
    - [B. Modification de l'unité](#b-modification-de-lunité)

## 0. Préparation de la machine

🌞 **Setup de deux machines Rocky Linux configurées de façon basique.**

Setup :

```bash
[it4@node1 ~]$ cat /etc/hostname
node1.tp1.b2

[it4@node1 ~]$ cat /etc/hosts
[...]
10.101.1.11 node1 node1.tp1.b2
10.101.1.12 node2 node2.tp1.b2

[it4@node1 ~]$ cat /etc/resolv.conf
nameserver 1.1.1.1

[it4@node1 ~]$ cat ~/.ssh/authorized_keys 
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMO/JQ3AtA3k8iXJWlkdUKSHDh215OKyLR0vauzD7BgA it4@nowhere.it4
```

Preuves :

```bash
# connexion SSH sans password
[it4@nowhere ~]$ ssh 10.101.1.11
Last login: Mon Nov 14 11:46:50 2022 from 10.101.1.1

# ping grâce au fichier /etc/hosts
[it4@node1 ~]$ ping node2
PING node2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2 (10.101.1.12): icmp_seq=1 ttl=64 time=1.09 ms
64 bytes from node2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.953 ms
^C
--- node2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.953/1.019/1.086/0.066 ms

# résolution d'un nom de domaine
[it4@node1 ~]$ dig google.com

; <<>> DiG 9.16.23-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 15745
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		15	IN	A	142.250.75.238

;; Query time: 25 msec
;; SERVER: 1.1.1.1#53(1.1.1.1) 🌞
;; WHEN: Mon Nov 14 11:52:18 CET 2022
;; MSG SIZE  rcvd: 55

# configuration du fw
[it4@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: 
  ports: 8888/tcp 22/tcp 🌞
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```


## I. Utilisateurs

[Une section dédiée aux utilisateurs est dispo dans le mémo Linux.](../../cours/memos/commandes.md#gestion-dutilisateurs).

### 1. Création et configuration

🌞 **Ajouter un utilisateur à la machine**

```bash
[it4@node1 ~]$ sudo useradd it5 -s /bin/bash -m -d /home/it5
[it4@node1 ~]$ sudo passwd it5 # on définit un password à cet utilisateur

[it4@node1 ~]$ cat /etc/passwd
[...]
it5:x:1001:1002::/home/it5:/bin/bash
```

🌞 **Créer un nouveau groupe `admins`**

```bash
[it4@node1 ~]$ sudo groupadd admins
[it4@node1 ~]$ sudo visudo
[it4@node1 ~]$ sudo cat /etc/sudoers | grep admin
%admins	ALL=(ALL)	ALL
```

🌞 **Ajouter votre utilisateur à ce groupe `admins`**

```bash
[it4@node1 ~]$ sudo usermod -aG admins it5
[it4@node1 ~]$ groups it5
it5 : it5 admins
```

Test :

```bash
[it4@node1 ~]$ su - it5
Password: 
Last login: Mon Nov 14 11:15:28 CET 2022 on pts/0
[it5@node1 ~]$ sudo -l
[sudo] password for it5: 
Matching Defaults entries for it5 on node1:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin, env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS",
    env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE", env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES", env_keep+="LC_MONETARY
    LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE", env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY", secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User it5 may run the following commands on node1:
    (ALL) ALL
```

### 2. SSH

🌞 **Pour cela...**

```bash
# clé déjà générée à l'aide d'une commande ssh-keygen sur le poste client

[it4@nowhere ~]$ ssh-copy-id 10.101.1.11
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
it4@10.101.1.11's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh '10.101.1.11'"
and check to make sure that only the key(s) you wanted were added.

[it4@nowhere ~]$ ssh 10.101.1.11
Last login: Mon Nov 14 11:59:58 2022 from 10.101.1.1
# pas de password demandé
```

## II. Partitionnement

🌞 **Utilisez LVM** pour...

```bash
# ajout des PV dans LVM
[it4@node1 ~]$ sudo pvcreate /dev/sdb
[it4@node1 ~]$ sudo pvcreate /dev/sdc

# création du groupe de volume (VG)
[it4@node1 ~]$ sudo vgcreate data /dev/sdb
[it4@node1 ~]$ sudo vgextend data /dev/sdc

# création des volumes logiques : nos partitions (LV)
[it4@node1 ~]$ sudo lvcreate -L 1G data --name part1
[it4@node1 ~]$ sudo lvcreate -L 1G data --name part2
[it4@node1 ~]$ sudo lvcreate -L 1G data --name part3

# formatage (= création du système de fichiers) sur chacune des partitions
[it4@node1 ~]$ sudo mkfs.ext4 /dev/data/part1 
[it4@node1 ~]$ sudo mkfs.ext4 /dev/data/part2
[it4@node1 ~]$ sudo mkfs.ext4 /dev/data/part3

# création des 3 points de montage
[it4@node1 ~]$ sudo mkdir /mnt/part{1,2,3}

# montage de chacun des partitions sur son point de montage
[it4@node1 ~]$ sudo mount /dev/data/part1  /mnt/part1
[it4@node1 ~]$ sudo mount /dev/data/part2  /mnt/part2
[it4@node1 ~]$ sudo mount /dev/data/part3  /mnt/part3
```

🌞 **Grâce au fichier `/etc/fstab`**, faites en sorte que cette partition soit montée automatiquement au démarrage du système.

```bash
[it4@node1 ~]$ cat /etc/fstab
[...]
/dev/data/part1     /mnt/part1                       ext4     defaults        0 0
/dev/data/part2     /mnt/part2                       ext4     defaults        0 0
/dev/data/part3     /mnt/part3                       ext4     defaults        0 0

[it4@node1 ~]$ sudo mount -av
[...]
/mnt/part1               : successfully mounted
[...]
/mnt/part2               : successfully mounted
[...]
/mnt/part3               : successfully mounted
[...]
```

## III. Gestion de services

## 1. Interaction avec un service existant

⚠️ **Uniquement sur `node1.tp1.b2`.**

Parmi les services système déjà installés sur Rocky, il existe `firewalld`. Cet utilitaire est l'outil de firewalling de Rocky.

🌞 **Assurez-vous que...**

- l'unité est démarrée
- l'unitée est activée (elle se lance automatiquement au démarrage)

## 2. Création de service

![Création de service systemd](./pics/create_service.png)

### A. Unité simpliste

⚠️ **Uniquement sur `node1.tp1.b2`.**

🌞 **Créer un fichier qui définit une unité de service** 

- le fichier `web.service`
- dans le répertoire `/etc/systemd/system`

Déposer le contenu suivant :

```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```

Le but de cette unité est de lancer un serveur web sur le port 8888 de la machine. **N'oubliez pas d'ouvrir ce port dans le firewall.**

Une fois l'unité de service créée, il faut demander à *systemd* de relire les fichiers de configuration :

```bash
$ sudo systemctl daemon-reload
```

Enfin, on peut interagir avec notre unité :

```bash
$ sudo systemctl status web
$ sudo systemctl start web
$ sudo systemctl enable web
```

🌞 **Une fois le service démarré, assurez-vous que pouvez accéder au serveur web**

- avec un navigateur depuis votre PC
- ou la commande `curl` depuis l'autre machine (je veux ça dans le compte-rendu :3)
- sur l'IP de la VM, port 8888

### B. Modification de l'unité

🌞 **Préparez l'environnement pour exécuter le mini serveur web Python**

- créer un utilisateur `web`
- créer un dossier `/var/www/meow/`
- créer un fichier dans le dossier `/var/www/meow/` (peu importe son nom ou son contenu, c'est pour tester)
- montrez à l'aide d'une commande les permissions positionnées sur le dossier et son contenu

> Pour que tout fonctionne correctement, il faudra veiller à ce que le dossier et le fichier appartiennent à l'utilisateur `web` et qu'il ait des droits suffisants dessus.

🌞 **Modifiez l'unité de service `web.service` créée précédemment en ajoutant les clauses**

- `User=` afin de lancer le serveur avec l'utilisateur `web` dédié
- `WorkingDirectory=` afin de lancer le serveur depuis le dossier créé au dessus : `/var/www/meow/`
- ces deux clauses sont à positionner dans la section `[Service]` de votre unité

🌞 **Vérifiez le bon fonctionnement avec une commande `curl`**
