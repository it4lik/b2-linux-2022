# Sujet Rattrapage B2

Ce sujet a pour but de vous faire retravailler quelques notions abordées pendant les cours de Réseau et de Linux.

Aucune nouvelle notion donc, simplement de la mise en place, avec des outils qu'on a déjà manipulés en cours. Partant de ce principe, le TP sera moins guidé, et vous devrez donc vous aider des [cours](../../cours/README.md) existants et des autres [TPs](../README.md).

> Gardez surtout le [mémo de commandes](../../cours/memos/commandes.md) et le [mémo réseau Rocky](../../cours/memos/rocky_network.md) sous le coude si ces dernières ne vous viennent pas naturellement. Toutes les commandes pour réaliser ce sujet s'y trouvent.

Toutes les machines Linux du TP :

- sont des machines virtuelles Rocky9
- sont configurées en suivant la [checklist](#checklist)

Votre compte-rendu doit :

- être au format Markdown
- figurer dans votre dépôt habituel de rendu dans un dossier `repech/`
- contenir TOUTES les commandes réalisées
- être le plus clean possible
  - vous ne copiez/collez pas mon sujet dans votre rendu please ?...
  - ça doit être lisible
  - vous listez toutes les étapes du setup
  - utilisation de balises de code pour vos lignes de commande

> Je serai plutôt intransigeant sur le caractère clean de votre compte-rendu, imposez-vous de la rigueur, et rendez-moi quelque chose de propre. Inutile de dire que j'attends un rendu original. Ha bah je l'ai dit.

## Sommaire

- [Sujet Rattrapage B2](#sujet-rattrapage-b2)
  - [Sommaire](#sommaire)
  - [Checklist](#checklist)
- [0. Le lab](#0-le-lab)
- [I. Setup](#i-setup)
  - [1. Emby server](#1-emby-server)
  - [2. Reverse proxy](#2-reverse-proxy)
    - [A. Setup](#a-setup)
    - [B. Secu](#b-secu)
  - [3. Backup](#3-backup)
    - [A. Serveur NFS](#a-serveur-nfs)
    - [B. Client NFS](#b-client-nfs)
    - [C. Script de backup](#c-script-de-backup)

## Checklist

![Checklist](./pics/checklist_is_here.jpg)

- [x] système à jour
- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel
- [x] accès Internet (une route par défaut, une carte NAT c'est très bien)
- [x] résolution de nom
- [x] SELinux activé en mode *"permissive"* (vérifiez avec `sestatus`, voir [mémo install VM tout en bas](https://gitlab.com/it4lik/b1-reseau-2022/-/blob/main/cours/memo/install_vm.md#4-pr%C3%A9parer-la-vm-au-clonage))

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# 0. Le lab

Vous allez avoir besoin de 3 machines :

- 🖥️ `web.peche.linux`
  - l'application [Emby](https://emby.media) sera déployée
  - c'est du streaming audio/vidéo
- 🖥️ `backup.peche.linux`
  - un serveur NFS sera installé
  - comme fait en cours, on s'en servira pour partager des dossiers à travers le réseau
  - il hébergera des sauvegardes
- 🖥️ `proxy.peche.linux`
  - cette machine porte un reverse proxy NGINX
  - il permet de protéger l'accès au serveur Web

# I. Setup

## 1. Emby server

> A faire sur 🖥️ `web.peche.linux`.

🌞 **Mettez en place le serveur Emby en suivant la doc officielle**

- vous pouvez sélectionner "CentOS", ça nous va, c'est très similaire à notre Rocky
- c'est une seule commande `yum` pour setup Emby normalement :)

🌞 **Une fois en place, mettez en évidence :**

- quel service est démarré (commande `systemctl`)
- comment consulter les logs de Emby (`journalctl` ou `cat`)
- quel processus sont lancés par ce service (commande `ps` et/ou `systemctl`)
- sur quel(s) port(s) ce(s) processus écoute(nt) (commande `ss`)
- avec quel utilisateur est lancé le(s) processus (commande `ps`)

🌞 **Accédez à l'interface Web**

- il sera nécessaire d'ouvrir un port firewall
- vous y accéder depuis le navigateur de votre PC avec : `http://<IP_VM>:<PORT>`
- vous pourrez y finaliser l'install

🌞 **Configurer un répertoire pour accueillir vos médias**

- créer un répertoire `/srv/media/`
- faites le appartenir au bon user (celui qui lance le serveur Emby : c'est lui qui aura besoin d'accéder aux données)
- donnez lui les permissions les plus restrictives possibles, le minimumm pour que tout fonctionne correctement
- uploadez un morceau de musique sur la VM (depuis votre PC) avec la commande `scp`
- sur l'interface Web de Emby, indiquez que vous avez une nouvelle bibliothèque musicale dans `/srv/media/`
- vous devriez pouvoir écouter votre morceau depuis la WebUI

> **Ne passez pas à la suite** tant que vous n'avez pas écouter le morceau depuis la WebUI :)

## 2. Reverse proxy

### A. Setup

> A faire sur 🖥️ `proxy.peche.linux`.

Ici, on mettre en place un reverse proxy qui protégera l'accès à notre serveur Web.

🌞 **Mettez en place NGINX**

- il faudra le configurer pour qu'il proxy vers Emby qui se trouve sur la machine `web.peche.linux`
- référez-vous au [module 1 du TP3](../3/1-reverse-proxy/README.md) pour + de détails
- vous mettrez en place du HTTPS
  - utilisez le nom `emby.peche.linux` pour le certificat
  - ce sera aussi le nom à utiliser pour visiter l'application
- vous accéderez donc par la suite à l'interface Web en tapant `https://emby.peche.linux` dans votre navigateur

> **Ne passez pas à la suite** tant que vous ne pouvez pas accéder à l'application Web en tapant `https://emby.peche.linux` dans votre navigateur sur votre PC.

### B. Secu

> A faire sur 🖥️ `web.peche.linux`.

🌞 **Ajoutez une règle firewall**

- l'interface Web doit être injoignable si on utilise l'IP de `web.peche.linux`
- on peut uniquement la joindre en passant par `proxy.peche.linux`
- vous devez donc ajouter une règle firewall pour bloquer le trafic qui vient d'une autre machine que `proxy.peche.linux` sur le port de l'interface Web

## 3. Backup

### A. Serveur NFS

> A faire sur 🖥️ `backup.peche.linux`.

Sur cette dernière machine, vous mettrez en place un serveur NFS.

🌞 **Mettez en place un serveur NFS**

- création d'un répertoire `/backups`
- création d'un répertoire `/backups/music/`
- référez-vous au [à la partie II du module 4 du TP3](../3/4-backup/README.md#ii-nfs) pour + de détails
  - vous devez partager à la machine `web.peche.linux` le dossier `/backup/music/`

### B. Client NFS

> A faire sur 🖥️ `web.peche.linux`.

🌞 **Mettez en place un client NFS**

- création d'un répertoire `/backup`
- référez-vous au [à la partie II du module 4 du TP3](../3/4-backup/README.md#ii-nfs) pour + de détails
  - vous devez monter le dossier `/backups/music/` qui se trouve sur `backup.peche.linux`
  - et le rendre accessible sur le dossier `/backup`

### C. Script de backup

> A faire sur votre machine (oui vous arrêtez de coder avec `vim` ou `nano` si vous maîtriser quedal et utilisez VSCode ou votre IDE préféré, comme d'hab). Le script doit s'exécuter sur `web.peche.linux`.

🌞 **Ecrivez un script de backup**

- langage `bash`
- il sera nommé `backup.sh` et stocké dans `/srv`
- vous utiliserez [Borg](https://www.borgbackup.org/) pour créer les sauvegardes

🌞 **Créez un *service* `backup.service`**

- il permet d'exécuter votre script de backup
- il s'exécute sous l'identité d'un utilisateur dédié : `backup` (nécessaire de le créer à la main au préalable donc)
- référez-vous au [TP1](../1/README.md) pour la création de *service*

🌞 **Créez un *timer* `backup.timer`**

- il exécute `backup.service` à intervalles réguliers
- plus précisément : `backup.service` est automatiquement lancé tous les matins à 07h00
