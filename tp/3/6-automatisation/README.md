# Module 6 : Automatiser le déploiement

Bon bah là je vais pas beaucoup écrire pour ce sujet.

C'est simple : automatiser via un script `bash` le TP2. Tout ce que vous avez fait à la main dans le TP2 pour déployer un NextCloud et sa base de données, le but ici est de le faire de façon automatisée *via* un script `bash`.

![Can it be ?](../pics/canitbe.png)

➜ **Ecrire le script `bash`**

- il s'appellera `tp3_automation_nextcloud.sh`
- le script doit commencer par un *shebang* qui indique le chemin du programme qui exécutera le contenu du script
  - ça ressemble à ça si on veut utiliser `/bin/bash` pour exécuter le contenu de notre script :

```
#!/bin/bash
```

- le script :
  - à partir d'une machine vierge (enfin, notre patron de cours Rocky quoi)
  - il installe complètement Apache et NextCloud
  - on lance le script, et on va prendre un café
- NB : le script ne comportera aucun `sudo` car il sera directement lancé avec les droits de `root`, question de bonnes pratiques :

```bash
$ sudo ./tp3_automation_nextcloud.sh
```

> Il peut être intéressant de tester la fonctionnalité de *snapshot* de VirtualBox afin de sauvegarder une VM de test dans un état vierge, et pouvoir la restaurer facilement dans cet état vierge plus tard.

✨ **Bonus** : faire un deuxième script `tp3_automation_db.sh` qui automatise l'installation de la base de données de NextCloud
