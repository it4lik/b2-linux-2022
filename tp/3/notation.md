| nom          | challenge | fonctionne ? | qualité doc | note |
|--------------|-----------|--------------|-------------|------|
| barême       | 10        | 5            | 5           | 20   |
| bedja        | 3         | 3            | 2           | 8    |
| biret        | 6         | 3            | 2           | 11   |
| bourmaud     | 8         | 5            | 4           | 18   |
| bousseria    | 6         | 5            | 4           | 15   |
| bravo        | 7         | 5            | 3           | 15   |
| brun         | 7         | 5            | 4           | 16   |
| chalaye      | 7         | 5            | 2           | 14   |
| corre        | 7         | 5            | 5           | 17   |
| coutant      | 0         | 0            | 0           | 0    |
| dalamel      | 7         | 5            | 4           | 16   |
| doublait     | 7         | 5            | 5           | 17   |
| duniaud      | 6         | 5            | 3           | 14   |
| eveillard    | 7         | 5            | 4           | 16   |
| ferreira a   | 7         | 5            | 3           | 15   |
| gadebille    | 7         | 5            | 4           | 16   |
| garcia f     | 8         | 5            | 4           | 17   |
| garcia l     | 6         | 5            | 5           | 16   |
| garrabos     | 7         | 5            | 2           | 14   |
| gilles       | 6         | 5            | 5           | 16   |
| grand morcel | 10        | 5            | 5           | 20   |
| guillet      | 9         | 5            | 1           | 15   |
| jovanovic    | 7         | 5            | 3           | 15   |
| juillard     | 7         | 5            | 5           | 17   |
| laroumanie   | 5         | 5            | 4           | 14   |
| lavaud       | 2         | 5            | 4           | 11   |
| latorre      | 8         | 5            | 2           | 15   |
| leconte      | 6         | 5            | 4           | 15   |
| lopez        | 10        | 0            | 0           | 10   |
| magne        | 0         | 0            | 0           | 0    |
| menard       | 7         | 5            | 2           | 15   |
| moiska       | 7         | 5            | 3           | 15   |
| morand       | 0         | 0            | 0           | 10   |
| pajak        | 10        | 5            | 5           | 20   |
| pery         | 5         | 5            | 1           | 11   |
| poirier      | 5         | 5            | 2           | 12   |
| porte        | 7         | 5            | 4           | 16   |
| pougeard     | 6         | 5            | 3           | 14   |
| renzema      | 10        | 5            | 5           | 20   |
| revol        | 7         | 4            | 3           | 14   |
| roulland     | 9         | 5            | 4           | 18   |
| selva        | 8         | 5            | 4           | 17   |
| senhaj       | 8         | 5            | 3           | 16   |
| thebault     | 7         | 5            | 3           | 15   |
| toribio      | 7         | 5            | 3           | 15   |
| toulemon     | 7         | 5            | 4           | 16   |
| vasseur      | 9         | 5            | 4           | 18   |
| vayssier     | 8         | 5            | 5           | 18   |
| weber        | 7         | 5            | 4           | 16   |
| weber        | 7         | 5            | 4           | 16   |
| ruemeli      | 4         | 5            | 2           | 11   |
